package com.gdstruc.quiz4;

public class Main {

                    public static void main(String[] args) {


                    Player a = new Player(1, "a", 99);
                    Player b = new Player(2, "bb", 99);
                    Player c = new Player(3, "ccc", 99);
                    Player d = new Player(4, "dddd", 99);
                    Player e = new Player(5, "eeeee", 99);
                    Player f = new Player(6, "fffff", 99);

                    SimpleHashTable table = new SimpleHashTable();

                    table.put(a.getUserName(), a);
                    table.put(b.getUserName(), b);
                    table.put(c.getUserName(), c);
                    table.put(d.getUserName(), d);
                    table.put(e.getUserName(), e);
                    table.put(f.getUserName(), f);

                    table.printHashTable();
                    System.out.println();
                    table.remove("fffff");
                    table.printHashTable();
                    //System.out.println(table.get("fffff"));



    }

}
