package com.gdstruc.quiz4;

public class StoredPlayer {
    public String key;
    public Player value;

    public StoredPlayer(String key, Player value) {
        this.key = key;
        this.value = value;
    }

    public String toString() {
        return "StoredPlayer(key :" + key + ", value:" + value + ")";
    }


}
