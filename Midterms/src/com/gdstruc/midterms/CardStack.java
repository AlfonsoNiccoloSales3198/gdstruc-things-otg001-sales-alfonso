package com.gdstruc.midterms;

import java.util.Objects;

public class CardStack {
    public CardNode head;

    public int cardCount = 0;

    public CardNode getHeadNode() {return this.head;}

    public CardNode getCard(int index)
    {
        CardNode temp = head;
        int i = 1;
        while(i != index)
        {
            temp = temp.getNextCard();
            i = i + 1;
        }
        return temp;
    }

    public int indexOf(Card needle){
        int count = -1;
        boolean found = false;
        CardNode trav = head;
        while (trav != null){
            if (trav.getCard().equals(needle))
                found = true;
            count = count + 1;
            trav = trav.getNextCard();
        }
        if (!found)
            count = -1;
        return count;
    }

    public boolean contains(Card needle){
        int count = -1;
        boolean found = false;
        CardNode trav = head;
        while (trav != null){
            if (trav.getCard().equals(needle))
                found = true;
            count = count + 1;
            trav = trav.getNextCard();
        }
        return found;
    }

    public void addToFront(Card card){
        CardNode cardNode = new CardNode(card);
        cardNode.setNextCard(head);
        head = cardNode;
        cardCount = cardCount + 1;
    }

    public void deleteFirst()
    {
        head = head.getNextCard();
        cardCount = cardCount - 1;
    }

    public int getElemCount()
    {
        return cardCount;
    }

    public void printList()
    {
        CardNode current = head;
        System.out.print("HEAD -> ");
        while (current != null)
        {
            System.out.print(current.getCard());
            System.out.print(" -> ");
            current = current.getNextCard();
        }
        System.out.println("null");
        System.out.println(cardCount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CardStack cardStack = (CardStack) o;
        return cardCount == cardStack.cardCount &&
                Objects.equals(head, cardStack.head);
    }

    @Override
    public int hashCode() {
        return Objects.hash(head, cardCount);
    }
}
