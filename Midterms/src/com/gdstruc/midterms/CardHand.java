package com.gdstruc.midterms;

import java.util.Objects;

public class CardHand {
    private CardHandNode head;

    public int cardCount = 0;

    public CardHandNode getHeadNode() {return this.head;}

    public CardHandNode getCard(int index)
    {
        CardHandNode temp = head;
        int i = 1;
        while(i != index)
        {
            temp = temp.getNextHandNode();
            i = i + 1;
        }
        return temp;
    }

    public int indexOf(Card needle){
        int count = -1;
        boolean found = false;
        CardHandNode trav = head;
        while (trav != null){
            if (trav.getCard().equals(needle))
                found = true;
            count = count + 1;
            trav = trav.getNextHandNode();
        }
        if (!found)
            count = -1;
        return count;
    }

    public boolean contains(Card needle){
        int count = -1;
        boolean found = false;
        CardHandNode trav = head;
        while (trav != null){
            if (trav.getCard().equals(needle))
                found = true;
            count = count + 1;
            trav = trav.getNextHandNode();
        }
        return found;
    }

    public void addToFront(Card card){
        CardHandNode cardHandNode = new CardHandNode(card);
        cardHandNode.setNextHandNode(head);
        head = cardHandNode;
        cardCount = cardCount + 1;
    }

    public void deleteFirst()
    {
        head = head.getNextHandNode();
        cardCount = cardCount - 1;
    }

    public int getElemCount()
    {
        return cardCount;
    }

    public void printList()
    {
        CardHandNode current = head;
        //System.out.print("HEAD -> \n");
        while (current != null)
        {
            System.out.print(current.getCard());
            System.out.print("\n");
            current = current.getNextHandNode();
        }
        //System.out.println("null");
        System.out.println(cardCount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CardHand cardHand = (CardHand) o;
        return cardCount == cardHand.cardCount &&
                Objects.equals(head, cardHand.head);
    }

    @Override
    public int hashCode() {
        return Objects.hash(head, cardCount);
    }
}
