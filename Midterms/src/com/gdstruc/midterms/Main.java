package com.gdstruc.midterms;

import java.lang.Math;

public class Main {

    public static void main(String[] args) {
        int turnCount = 1;
        CardStack cardStack = new CardStack();

        CardHand cardHand = new CardHand();
        CardStack discardCards = new CardStack();

        Card teleport1 = new Card(1, "Teleport");
        Card regen1 = new Card(2, "Regeneration");
        Card staff1 = new Card(3, "Staff");
        Card villager1 = new Card(4, "Villager");
        Card CobbleStone1 = new Card(5, "Cobble Stone");
        Card Mugic1 = new Card(6, "Mugic");
        Card Land1 = new Card(7, "Land");
        Card Vortex1 = new Card(8, "Vortex");


        cardStack.addToFront(teleport1);
        cardStack.addToFront(teleport1);
        cardStack.addToFront(teleport1);
        cardStack.addToFront(regen1);
        cardStack.addToFront(staff1);
        cardStack.addToFront(villager1);
        cardStack.addToFront(teleport1);
        cardStack.addToFront(CobbleStone1);
        cardStack.addToFront(Mugic1);
        cardStack.addToFront(villager1);
        cardStack.addToFront(Land1);
        cardStack.addToFront(Land1);
        cardStack.addToFront(Land1);
        cardStack.addToFront(Land1);
        cardStack.addToFront(Land1);
        cardStack.addToFront(teleport1);
        cardStack.addToFront(teleport1);
        cardStack.addToFront(teleport1);
        cardStack.addToFront(regen1);
        cardStack.addToFront(staff1);
        cardStack.addToFront(villager1);
        cardStack.addToFront(teleport1);
        cardStack.addToFront(CobbleStone1);
        cardStack.addToFront(Mugic1);
        cardStack.addToFront(villager1);
        cardStack.addToFront(Vortex1);
        cardStack.addToFront(Vortex1);
        cardStack.addToFront(Vortex1);
        cardStack.addToFront(Vortex1);
        cardStack.addToFront(Vortex1);


        while (cardStack.getElemCount() != 0)
        {
            System.out.println("Turn # " + turnCount);


            double rand = Math.random();

            if (rand > 0.0 && rand <= 0.33)
            {
                DrawX(cardStack, cardHand, 5);
                System.out.println("After adding cards to hand");
                checkCards(cardStack, cardHand, discardCards);
            }
            else if (rand > 0.33 && rand <= 0.66)
            {
                DiscardX(discardCards, cardHand, 3);
                System.out.println("After discarding cards in hand");
                checkCards(cardStack, cardHand, discardCards);
            }
            else if (rand > 0.66 && rand <= 1.0)
            {
                GetDiscardX(discardCards, cardHand, 1);
                System.out.println("After getting a card in discard");
                checkCards(cardStack, cardHand, discardCards);
            }

            turnCount += 1;

        }

    }

public static void checkCards(CardStack deck, CardHand hand, CardStack discard)
{
    System.out.println("Hand cards: ");
    hand.printList();
    System.out.println("Number of cards remaining in deck: " + deck.getElemCount());
    System.out.println("Number of cards in discard pile: " + discard.getElemCount());
}
public static void DrawX(CardStack deck, CardHand hand, int num)
{
    int temp = 0;
    int end = 0;
    if (deck.getElemCount() > 0)
    {
        if (num <= deck.getElemCount())
        {
            end = num;
        }
        else if (num > deck.getElemCount()) {
            end = deck.getElemCount();
        }
        while (temp != end)
        {
            Card curNode = deck.getHeadNode().getCard();
            hand.addToFront(curNode);
            deck.deleteFirst();
            temp = temp + 1;
        }
    }else System.out.println("Cannot draw from deck to hand");
}
public static void DiscardX(CardStack discard, CardHand hand, int num)
{
    int temp = 0;
    int end = 0;
    if (hand.getElemCount() > 0) {
        if (num <= hand.getElemCount()) {
            end = num;
        } else if (num > hand.getElemCount()) {
            end = hand.getElemCount();
        }
        while (temp != end) {
            Card curNode = hand.getHeadNode().getCard();
            discard.addToFront(curNode);
            hand.deleteFirst();
            temp = temp + 1;
        }
    } else System.out.println("Cannot discard from hand to discard pile");
}
public static void GetDiscardX(CardStack discard, CardHand hand, int num)
{
    int temp = 0;
    int end = 0;
    if (discard.getElemCount() > 0) {
        if (num <= discard.getElemCount()) {
            end = num;
        } else if (num > discard.getElemCount()) {
            end = discard.getElemCount();
        }
        while (temp != end) {
            Card curNode = discard.getHeadNode().getCard();
            hand.addToFront(curNode);
            discard.deleteFirst();
            temp = temp + 1;
        }
    }
}
   

}
