package com.gdstruc.midterms;


public class CardHandNode {
    private Card card;
    private CardHandNode nextHandNode;

    public CardHandNode(Card card) { this.card = card;}

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public CardHandNode getNextHandNode() {
        return nextHandNode;
    }

    public void setNextHandNode(CardHandNode nextHandNode) {
        this.nextHandNode = nextHandNode;
    }
}
