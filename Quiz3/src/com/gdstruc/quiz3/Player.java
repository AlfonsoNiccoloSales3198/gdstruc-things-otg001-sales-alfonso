package com.gdstruc.quiz3;

import java.util.Objects;

public class Player {
    private int PlayerID;
    private String userName;
    private int level;
    public Player(){};

    public Player(int id, String name, int level) {
        this.PlayerID = id;
        this.userName = name;
        this.level = level;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return PlayerID == player.PlayerID &&
                level == player.level &&
                Objects.equals(userName, player.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(PlayerID, userName, level);
    }

    @Override
    public String toString() {
        return "Player{" +
                "PlayerID=" + PlayerID +
                ", userName=" + userName +
                ", level=" + level +
                '}';
    }


}
