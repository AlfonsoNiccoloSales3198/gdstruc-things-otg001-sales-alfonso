package com.gdstruc.module2.quiz;


//import java.util.ArrayList;
//import java.util.List;

public class Main {
    public static void main(String[] args)
    {
        //List<Player> playerList = new ArrayList<>();

        Player asuna = new Player(1, "Asuna", 100);
        Player lethalBacon = new Player(2, "LethalBacon", 205);
        Player hpDeskjet = new Player(3, "HPDeskjet", 34);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        playerLinkedList.addToFront(asuna);
        playerLinkedList.addToFront(lethalBacon);
        playerLinkedList.addToFront(hpDeskjet);

        playerLinkedList.printList();

        playerLinkedList.deleteFirst();

        playerLinkedList.printList();
        //System.out.println(playerList.get(1));

        //playerList.add(2, new Player(553, "Arctis", 55));

        //playerList.remove(2);
        
        System.out.println(playerLinkedList.contains(new Player(2, "LethalBacon", 205)));
        System.out.println(playerLinkedList.indexOf(new Player(2, "LethalBacon", 205)));
        //playerList.forEach(player -> System.out.println(player));
        //for (Player p : playerList)
        //{
        //     System.out.println(p);
        //}
    }
}
