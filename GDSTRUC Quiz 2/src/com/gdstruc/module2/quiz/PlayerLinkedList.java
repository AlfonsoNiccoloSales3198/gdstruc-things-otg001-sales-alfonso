package com.gdstruc.module2.quiz;

import java.util.Objects;

public class PlayerLinkedList {
    private PlayerNode head;

    private int playerCount = 0;

    public int indexOf(Player needle){
        int count = -1;
        boolean found = false;
        PlayerNode trav = head;
        while (trav != null){
            if (trav.getPlayer().equals(needle))
                found = true;
            count = count + 1;
            trav = trav.getNextPlayer();
        }
        if (!found)
            count = -1;
        return count;
    }

    public boolean contains(Player needle){
        int count = -1;
        boolean found = false;
        PlayerNode trav = head;
        while (trav != null){
            if (trav.getPlayer().equals(needle))
                found = true;
            count = count + 1;
            trav = trav.getNextPlayer();
        }
        return found;
    }

    public void addToFront(Player player){
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
        playerCount = playerCount + 1;
    }

    public void deleteFirst()
    {
        head = head.getNextPlayer();
        playerCount = playerCount - 1;
    }

    public int getElemCount()
    {
        return playerCount;
    }

    public void printList()
    {
        PlayerNode current = head;
        System.out.print("HEAD -> ");
        while (current != null)
        {
            System.out.print(current.getPlayer());
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
        System.out.println(playerCount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayerLinkedList that = (PlayerLinkedList) o;
        return playerCount == that.playerCount &&
                Objects.equals(head, that.head);
    }

    @Override
    public int hashCode() {
        return Objects.hash(head, playerCount);
    }
}
