package com.gdstruc.quiz6;

public class Main {

        public static void main(String[] args) {


                Tree t = new Tree();

                t.insert(25);
                t.insert(17);
                t.insert(29);
                t.insert(18);
                t.insert(16);
                t.insert(-5);
                t.insert(10);
                t.insert(60);

                t.traverseInOrderDescending();
                t.traverseInOrder();

                //System.out.println(t.getMax().toString());

        }

}
