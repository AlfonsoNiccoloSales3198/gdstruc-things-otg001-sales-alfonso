package com.gdstruc.quiz6;

public class Node {
    private int data;
    private Node rightChild;
    private Node leftChild;

    public Node(int data) {
        this.data = data;
    }

    public void insert (int value) {
        if (value == data) {
            return;
        }

        if (value < data) {
            if (leftChild == null)
            {
                leftChild = new Node(value);
            }
            else leftChild.insert(value);
        } else {
            if (rightChild == null)
            {
                rightChild = new Node(value);
            }
            else rightChild.insert(value);
        }
    }

    public void traverseInOrder() {
        if (leftChild != null) {
            leftChild.traverseInOrder();
        }
        System.out.println("Data: " + data);
        if (rightChild != null) {
            rightChild.traverseInOrder();
        }
    }

    public void traverseInOrderDescending() {
        if (rightChild != null) {
            rightChild.traverseInOrderDescending();
        }
        System.out.println(toString());
        if (leftChild != null) {
            leftChild.traverseInOrderDescending();
        }
    }

    public Node get(int value) {
        if (data == value) {
            return this;
        }

        if (value < data) {
            if (leftChild != null) {
                return leftChild.get(value);
            }
        } else {
            if (rightChild != null) {
                return rightChild.get(value);
            }
        }

        return null;
    }

    public int getData() {
        return data;
    }

    public Node getRightChild() {
        return rightChild;
    }

    public Node getLeftChild() {
        return leftChild;
    }

    public void setData(int data) {
        this.data = data;
    }

    public void setRightChild(Node rightChild) {
        this.rightChild = rightChild;
    }

    public void setLeftChild(Node leftChild) {
        this.leftChild = leftChild;
    }

    public String toString() {
        return "Node(data: " + data + ")";
    }
}
