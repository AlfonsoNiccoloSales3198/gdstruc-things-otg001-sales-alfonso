package com.gdstruc.quiz6;

public class Tree {
    private Node rootNode;

    public Tree() {}

    public Tree(Node rootnode) {
        this.rootNode = rootnode;
    }

    public void setRootNode(Node rootNode) {
        this.rootNode = rootNode;
    }

    public Node getRootNode() {
        return rootNode;
    }

    public void insert (int value) {
        if (rootNode == null) {
            rootNode = new Node(value);
        } else {
            rootNode.insert(value);
        }
    }

    public void traverseInOrder() {
        if (rootNode != null) {
            rootNode.traverseInOrder();
        }

    }

    public Node get(int value) {
        if (rootNode != null) {
            return rootNode.get(value);
        }
        return null;
    }

    public Node getMin() {
        Node check = rootNode;
        while (check != null) {
            if (check.getLeftChild() != null)
                check = check.getLeftChild();
            else break;
        }
        return check;
    }

    public Node getMax() {
        Node check = rootNode;
        while (check != null) {
            if (check.getRightChild() != null)
                check = check.getRightChild();
            else break;
        }
        return check;
    }

    public void traverseInOrderDescending() {
        if (rootNode != null) {
            rootNode.traverseInOrderDescending();
        }
    }



}
