package com.gdstruc.module1.quiz;

public class Main {

    public static void main(String[] args) {

                int[] numbers = new int[10];

                numbers[0] = 35;
                numbers[1] = 69;
                numbers[2] = 1;
                numbers[3] = 10;
                numbers[4] = -50;
                numbers[5] = 320;
                numbers[6] = 63;
                numbers[7] = 58;
                numbers[8] = 26;
                numbers[9] = 13;

        System.out.println("Before sorting:");
        printArrayElements(numbers);

        //60pts task here comment out if needed to test the others
        selectionSortSmall(numbers);
        System.out.println("\n\nAfter modified selection sort smallest:");
        printArrayElements(numbers);

        //40pts tasks here comment out if needed to test the others
        bubbleSortRev(numbers);
        System.out.println("\n\nAfter modified bubble sort:");
        printArrayElements(numbers);


        selectionSortRev(numbers);
        System.out.println("\n\nAfter modified selection sort:");
        printArrayElements(numbers);



    }
    //modified to sort in descending order
    private static void bubbleSortRev(int[] arr)
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            for (int i = 0; i < lastSortedIndex; i++)
            {
                if(arr[i] < arr[i+1])
                {
                    int temp = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = temp;
                }
            }
        }
    }


    private static void selectionSortRev(int[] arr)
    {
        for (int i = 0; i < arr.length; i++)
        {
            for (int j = i+1; j< arr.length; j++)
            {
                if (arr[i] < arr[j])
                {
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

    private static void selectionSortSmall(int[] arr)
    {
        for (int i = arr.length -1; i >=0; i--)
        {
            for (int j = 0; j<i; j++)
            {
                if (arr[i] > arr[j])
                {
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

    private static void printArrayElements(int[] arr) {
        for (int j : arr) {
            System.out.print(j + " ");
        }
    }
}
